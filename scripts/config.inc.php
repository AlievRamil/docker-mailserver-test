<?php

/* Local configuration for Roundcube Webmail */

// SQL DATABASE
$config['db_dsnw'] = 'mysql://roundcube:Password123@mysql/roundcubemail';
// LOGGING/DEBUGGING
$config['debug_level'] = 5;
// IMAP
$config['default_host'] = 'mail';
// SMTP
$config['smtp_server'] = 'mail';
$config['smtp_user'] = '%u';
$config['smtp_pass'] = '%p';
$config['support_url'] = '';
$config['des_key'] = 'GScgGuKeXtTFeRXZGtP0Tj5Z';
$config['username_domain'] = 'ramil.com';
// PLUGINS
$config['plugins'] = array();
$config['language'] = 'en_US';
$config['enable_spellcheck'] = false;
$config['spellcheck_engine'] = 'pspell';
$config['skin'] = 'mabola';