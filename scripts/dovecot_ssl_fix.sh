#!bin/bash

/bin/sed -i -e 's/ssl = required/''/g' /etc/dovecot/conf.d/10-ssl.conf
/bin/echo "#ssl = required" >> /etc/dovecot/conf.d/10-ssl.conf
/bin/echo "disable_plaintext_auth = no" >> /etc/dovecot/conf.d/10-auth.conf
/etc/init.d/dovecot reload