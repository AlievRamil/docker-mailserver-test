#! /bin/sh

cp /scripts/.my.cnf /root/.my.cnf
sleep 5
mysql --defaults-extra-file=/root/.my.cnf -Bse 'CREATE DATABASE roundcubemail;'
mysql --defaults-extra-file=/root/.my.cnf -Bse "GRANT ALL PRIVILEGES ON roundcubemail.* TO 'roundcube'@'localhost' IDENTIFIED BY 'Password123';"
mysql --defaults-extra-file=/root/.my.cnf -Bse "GRANT ALL PRIVILEGES ON roundcubemail.* TO 'roundcube'@'%' IDENTIFIED BY 'Password123';"
mysql --defaults-extra-file=/root/.my.cnf -Bse 'FLUSH PRIVILEGES;'
mysql -uroot -pPassword123 roundcubemail < /scripts/mysql.initial.sql