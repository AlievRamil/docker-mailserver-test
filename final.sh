#!/bin/bash

#docker exec roundcube /scripts/roundcube.sh
docker exec roundcube cp /scripts/config.inc.php /var/www/config/
docker exec roundcube chown -fR www-data:www-data /var/www/config/
sleep 30
docker exec mail /scripts/dovecot_ssl_fix.sh
docker exec mysql /scripts/roundcube_mysql.sh
